import React, { useState, useEffect } from 'react';
import './App.css';

function App() {
  const [elapsed, setElapsed] = useState(0)
  const [duration, setDuration] = useState(0)
  
  useEffect(() => {
    const intervalID = setInterval(() => {
      if (elapsed < duration) {
        setElapsed(elapsed + 1)
      }
    }, 1000);
    
    return () => clearInterval(intervalID)
  }, [elapsed, duration])

  const handleSliderChange = (event) => {
    setDuration(event.target.value)
  }

  const reset = () => {
    setElapsed(0)
    setDuration(0)
  }

  return (
    <div className={['Container', 'horizontally-centered-flex'].join(' ')}>
      <div className="bar">
        <h4>Elapsed Time: {elapsed}</h4>
        <div className="elapsed-bar-container">
          <div className="elapsed-bar" style={{width: `${isNaN(elapsed/duration) ? 0 : elapsed/duration * 100}%`}}></div>
        </div>
      </div>
      <div className="bar">
        <h4>Duration: {duration} sec</h4>
        <input 
          type="range" 
          min="0" max="60" 
          value={duration}
          onChange={handleSliderChange}
          step="1"
        />
      </div>
      <button onClick={reset}><h4>Reset</h4></button>
    </div>
  );
}

export default App;